def reportFileLocation = 'report.html'

def reportResult(reportFile) {
  publishHTML([
    allowMissing: false,
    alwaysLinkToLastBuild: false,
    keepAll: false,
    reportDir: '',
    reportFiles: reportFile,
    reportName: 'API Report'
  ])
}

node {
  stage("Clone") {
    git "https://gitlab.com/martin.gawlik/testsuite_new.git"
  }

  stage("Test") {
    withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: "gitlab", usernameVariable: 'GITLAB_USER', passwordVariable: 'GITLAB_PW']]) {
      withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: "gitlab_token", usernameVariable: 'GITLAB_USER', passwordVariable: 'GITLAB_TOKEN']]) {
        try {
          sh "pytest tests/api_test.py --html=${reportFileLocation}"
        } catch(err) {
          reportResult(reportFileLocation)
          error "${err}"
        }
      }
    }
  }

  stage("Report") {
    reportResult(reportFileLocation)
  }
}